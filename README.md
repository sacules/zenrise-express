# Zenrise Express

## Compilado
```console
npm install
npm start
```

## Rediseño
Además de remaquetar la pantalla de Servicios, se hicieron algunos pequeños ajustes:

* Se dividió en 4 secciones: Monto, Contacto, Vencimientos y Cargos, y Detalle para ayudar a diferenciar
las diferentes acciones que son posible realizar en la página
* La dirección del degradé en el título fue invertida (de amarillo -> naranja a naranja -> amarillo) para mejorar
el contraste y legibilidad
* Se removió el texto "Necesitamos un monto..." del principio al ser redundante
* `Enviar mail` tiene mejor contraste y está correctamente alineado
* Los recargos tienen su propia caja y una label que indica que son recargos
* Se agregó un título al área del texto personalizado para mantener la consistencia
* El botón de `Enviar cobro Express` ahora es grisáceo hasta que se cumplan las condiciones para poder realizar el
cobro, entonces se torna de color verde
* Se quitó el padding a los contactos para mantener consistencia con los demás componentes

## Limitaciones
Debido a limitaciones en la forma de personalizar que permite la librería de UI elegida (Semantic UI), además
por falta de tiempo, se tiene en cuenta que:

* Los bordes curvos son de un radio de 0.3rem en vez de 25px
* Los input no validan la entrada por lo que es posible asignar letras como valores
* Los input tienen un borde celeste en vez de naranja al ser seleccionados
* Hay un bug que permite agregar varias veces a un mismo contacto
* Los íconos no tienen `alt`
* No ha sido posible centrar el input de monto
* Varios colores no han posido ser cambiados

## Interacción con backend
Sólo hay 2 interacciones: cuando se consigue la lista de contactos, y al enviar la data para realizar el cobro. Ésta última se manda como paramétros a través de un POST. También se podría empaquetar todo en un solo JSON como payload.
