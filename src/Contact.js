import { Button, Form, Modal } from "semantic-ui-react";

export class Contact {
  constructor(name, surname, email) {
    this.name = name;
    this.surname = surname;
    this.email = email;
  }
}

export const EditContact = ({
  open,
  setOpen,
  selectedContact,
  contacts,
  setContacts,
}) => {
  if (contacts.length === 0) {
    return "";
  }

  let contact = contacts[selectedContact];

  return (
    <Modal
      closeIcon
      open={open}
      onOpen={() => setOpen(true)}
      onClose={() => setOpen(false)}
    >
      <Modal.Header>Editar contacto</Modal.Header>
      <Modal.Content>
        <Form>
          <Form.Input
            label="Nombre"
            type="text"
            defaultValue={contact.name}
            onChange={(_, { value }) => {
              contact.name = value;
            }}
          />
          <Form.Input
            label="Apellido"
            defaultValue={contact.surname}
            onChange={(_, { value }) => {
              contact.surname = value;
            }}
          />
          <Form.Input
            label="Email (opcional)"
            defaultValue={contact.email}
            onChange={(_, { value }) => {
              contact.email = value;
            }}
          />

          <div className="contact-edit-buttons">
            <Button
              floated="right"
              onClick={() => {
                setOpen(false);
              }}
            >
              Cancelar
            </Button>

            <Button
              floated="right"
              positive
              onClick={() => {
                contacts[selectedContact] = contact;
                setContacts(contacts);
                setOpen(false);
              }}
            >
              Confirmar
            </Button>
          </div>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

export const ContactCard = ({ i, contact, setSelected, remove, setOpen }) => {
  const { name, surname, email } = contact;

  return (
    <div className="contact">
      <div className="contact-letter">{name.toUpperCase().charAt(0)}</div>
      <div className="contact-details">
        <div className="contact-details2">
          <p className="text-regular">
            {surname}, {name}
          </p>

          <p className="text-small">
            {email === "" ? "Contacto sin mail asociado" : email}
          </p>
        </div>

        <div className="contact-buttons">
          <Button
            basic
            circular
            icon="edit"
            onClick={() => {
              setSelected(i);
              setOpen(true);
            }}
          />
          <Button basic circular icon="close" onClick={() => remove(contact)} />
        </div>
      </div>
    </div>
  );
};
