import axios from "axios";
import MockAdapter from "axios-mock-adapter";
import { Contact } from "./Contact";

export const user_id = "ekilibrium1";

const defaultContacts = [
  new Contact("Juan", "Lopez", ""),
  new Contact("Pedro", "Perez", "pedro@fakemail.com"),
];

var mock = new MockAdapter(axios);

mock.onGet("/contacts", { params: { user_id: user_id } }).reply(200, {
  contacts: defaultContacts,
});
