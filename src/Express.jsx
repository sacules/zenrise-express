import axios from "axios";
import { useState } from "react";
import {
  Checkbox,
  Button,
  Dropdown,
  Form,
  Header,
  Input,
  TextArea,
} from "semantic-ui-react";
import SemanticDatepicker from "react-semantic-ui-datepickers";
import "react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css";

import { Contact, EditContact, ContactCard } from "./Contact";

const AmountSection = ({ setMoney, setHasMoney }) => {
  const [error, setError] = useState("");

  return (
    <Form.Field>
      <Header as="h2" dividing>
        Monto
      </Header>
      <div className="amount">
        <Input
          transparent
          type="number"
          step="any"
          placeholder="0,00"
          icon="dollar"
          iconPosition="left"
          size="massive"
          className="amount-input"
          onChange={(_, { value }) => {
            const val = parseFloat(value);
            if (val >= 10) {
              setHasMoney(true);
              setError("");
            } else {
              setHasMoney(false);
              setError("El monto mínimo es de $10");
            }

            if (isNaN(val)) {
              return;
            }

            setMoney(val);
          }}
        />
      </div>
      <div className="error">{error}</div>
    </Form.Field>
  );
};

const ContactsSection = ({
  user_id,
  contacts,
  setContacts,
  setHasContact,
  setSendMail,
}) => {
  const [open, setOpen] = useState(false);
  const [selectedContact, setSelectedContact] = useState(0);
  const [options, setOptions] = useState([]);
  const [error, setError] = useState("");

  const handleRemoveContact = (contact) => {
    setContacts(contacts.filter((item) => item !== contact));
  };

  return (
    <div>
      <Header as="h2" dividing>
        Contacto
      </Header>

      <EditContact
        open={open}
        setOpen={setOpen}
        selectedContact={selectedContact}
        contacts={contacts}
        setContacts={setContacts}
      />

      <Dropdown
        placeholder="Comienza a escribir para seleccionar o crear un contacto..."
        fluid
        search
        options={options.map((c) => {
          return { value: c, text: c.surname + ", " + c.name };
        })}
        noResultsMessage={null}
        selection
        allowAdditions
        onChange={(_, { value }) => {
          setContacts([value, ...contacts]);
          setSelectedContact(0);
          setHasContact(true);
          setError("");
        }}
        onAddItem={(_, { value }) => {
          setContacts([new Contact(value, "", ""), ...contacts]);
          setSelectedContact(0);
          setOpen(true);
          setHasContact(true);
          setError("");
        }}
        onOpen={() =>
          axios
            .get("/contacts", { params: { user_id: user_id } })
            .then((resp) => {
              setOptions(resp.data.contacts);
            })
        }
        onBlur={() => {
          if (contacts.length === 0) {
            setError("1 contacto como mínimo");
          }
        }}
      />

      <ul>
        {contacts.map((c, i) => {
          return (
            <li>
              <ContactCard
                i={i}
                contact={c}
                setSelected={setSelectedContact}
                remove={handleRemoveContact}
                setOpen={setOpen}
              />
            </li>
          );
        })}

        <div className="error">{error}</div>

        <li>
          <Checkbox
            label="Enviar email"
            onClick={(_, { value }) => setSendMail(value)}
          />
        </li>
      </ul>
    </div>
  );
};

const ExpirationCard = ({ label, setExpiration }) => {
  const [date, setDate] = useState(null);
  const [percent, setPercent] = useState(0);

  return (
    <div>
      <Form.Group widths="equal">
        <SemanticDatepicker
          label={label}
          onChange={(_, { value }) => {
            setDate(value);
            setExpiration({ date: value, percent: percent });
          }}
        />
        <Form.Input
          type="number"
          label="Recargo"
          placeholder="0,00"
          icon="percent"
          onChange={(_, { value }) => {
            setPercent(value);
            setExpiration({ date: date, percent: value });
          }}
        />
      </Form.Group>
    </div>
  );
};

const ExpirationSection = ({ setFirstExpiration, setSecondExpiration }) => {
  return (
    <div>
      <Header as="h2" dividing>
        Vencimientos y cargos
      </Header>
      <ExpirationCard
        label="Primer vencimiento"
        setExpiration={setFirstExpiration}
      />
      <ExpirationCard
        label="Segundo vencimiento"
        setExpiration={setSecondExpiration}
      />
    </div>
  );
};

export const Express = ({ user_id }) => {
  const [money, setMoney] = useState(0);
  const [hasMoney, setHasMoney] = useState(false);

  const [contacts, setContacts] = useState([]);
  const [hasContact, setHasContact] = useState(false);
  const [sendMail, setSendMail] = useState(false);

  const [firstExpiration, setFirstExpiration] = useState({});
  const [secondExpiration, setSecondExpiration] = useState({});

  const [personalText, setPersonalText] = useState("");

  const handleSubmit = () => {
    if (!(hasMoney && hasContact)) {
      return;
    }

    axios
      .post("/express", {
        amount: money,
        contacts: contacts,
        sendMail: sendMail,
        firstExpiration: firstExpiration,
        secondExpiration: secondExpiration,
        personalText: personalText,
      })
      .catch((e) => console.log(e));
  };

  return (
    <div>
      <AmountSection setMoney={setMoney} setHasMoney={setHasMoney} />
      <ContactsSection
        user_id={user_id}
        contacts={contacts}
        setSendMail={setSendMail}
        setContacts={setContacts}
        setHasContact={setHasContact}
      />

      <ExpirationSection
        setFirstExpiration={setFirstExpiration}
        setSecondExpiration={setSecondExpiration}
      />

      <Header as="h2" dividing>
        Detalle
      </Header>

      <label as="h5" className="personal-text">
        Escriba un texto personalizado...
      </label>
      <TextArea
        placeholder="Escriba aquí..."
        onChange={(_, { value }) => setPersonalText(value)}
      />

      <div className="payment">
        <Button toggle active={hasMoney && hasContact} onClick={handleSubmit}>
          Enviar cobro Express
        </Button>
      </div>
    </div>
  );
};
