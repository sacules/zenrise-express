import React from "react";
import { Form, Tab, Container, Header } from "semantic-ui-react";

import "normalize.css";
import "semantic-ui-css/semantic.min.css";

import { Express } from "./Express";
import { user_id } from "./requests";

const panes = [
  {
    menuItem: "Servicios",
    render: () => (
      <Tab.Pane>
        <Express user_id={user_id} />
      </Tab.Pane>
    ),
  },
  {
    menuItem: "Items Precargados",
    render: () => <Tab.Pane>No Implementado</Tab.Pane>,
  },
];

function App() {
  return (
    <main>
      <Container text>
        <Form className="form-header">
          <Header as="h1" className="main-header">
            Realizar Cobro Express
          </Header>
          <Tab panes={panes} />
        </Form>
      </Container>
    </main>
  );
}

export default App;
